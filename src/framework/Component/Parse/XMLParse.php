<?php

namespace mpwarframework\framework\Component\Parse;


use SimpleXMLElement;

class XMLParse implements Parse
{
    public function __construct()
    {

    }

    public function parse($a_file_name)
    {
        $xml = simplexml_load_string($a_file_name);
        $array = $this->XML2Array($xml);
        $array = array($xml->getName() => $array);
        return $array;
    }

    private function XML2Array(SimpleXMLElement $parent)
    {
        $array = array();

        foreach ($parent as $name => $element) {
            ($node = &$array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = &$node[];

            $node = $element->count() ? $this->XML2Array($element) : trim($element);
        }

        return $array;
    }


}