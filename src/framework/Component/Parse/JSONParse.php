<?php

namespace mpwarframework\framework\Component\Parse;


class JSONParse implements Parse
{
    public function __construct()
    {

    }

    public function parse($a_file_name)
    {
        $obj = json_decode($a_file_name);
        return $obj;
    }
}