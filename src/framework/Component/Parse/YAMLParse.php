<?php

namespace mpwarframework\framework\Component\Parse;


use Symfony\Component\Yaml\Parser;

class YAMLParse implements Parse
{

    public function __construct()
    {

    }

    public function parse($a_file_name)
    {
        $config = array();
        $yaml = new Parser();
        $config = $yaml->parse(file_get_contents($a_file_name));
        return $config;
    }
}