<?php

namespace mpwarframework\framework\Component\View;

use Smarty;

class SmartyView implements View
{

    private $smarty;
    private $data = [];


    public function __construct($templatesPath)
    {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir($templatesPath);
    }

    public function show($template, $params = null)
    {
        $template = $template . '.tpl';
        if (!is_null($params)) {
            foreach ($params as $paramName => $value) {
                $this->smarty->assign($paramName, $value);
            }
        } else {
            if (!is_null($this->data)) {
                foreach ($this->data as $paramName => $value) {
                    $this->smarty->assign($paramName, $value);
                }
            }
        }

        $this->smarty->display($template);
    }

    public function assign($name, $value)
    {
        $this->data[$name] = $value;
    }


}