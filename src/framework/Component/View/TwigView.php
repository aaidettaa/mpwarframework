<?php

namespace mpwarframework\framework\Component\View;


use Twig_Environment;
use Twig_Loader_Filesystem;

class TwigView implements View
{

    private $environment;
    private $data = [];


    public function __construct($templatesPath)
    {
        $loader = new Twig_Loader_Filesystem($templatesPath);
        $this->environment = new Twig_Environment($loader);
    }

    public function show($template, $params = null)
    {
        $template = $template . '.twig';
        if (!is_null($params)) {
            return $this->environment->render($template, $params);
        } else {
            if (!is_null($this->data)) {
                return $this->environment->render($template, $this->data);
            }
        }
        return $this->environment->render($template);
    }

    public function assign($name, $value)
    {
        $this->data[$name] = $value;
    }


}