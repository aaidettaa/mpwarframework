<?php

namespace mpwarframework\framework\Component\View;

interface View
{
    public function show($template, $params = null);

    public function assign($name, $value);
}