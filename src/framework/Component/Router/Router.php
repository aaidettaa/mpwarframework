<?php

namespace mpwarframework\framework\Component\Router;


class Router
{

    private $routes;

    public function __construct($arr_routes)
    {
        $this->routes = Array();
        foreach ($arr_routes as $route) {
            $controller = $route['controller']['class'];
            $uri = $route['path'];
            $action = $route['controller']['action'];
            $routeObj = new Route($uri, $controller, $action);
            array_push($this->routes, $routeObj);
        }
    }


    public function getRoute($path)
    {
        if (preg_match('/app.php\//', $path)) {
            $path = str_replace("app.php/", "", $path);
        }
        if (preg_match('/app.php/', $path)) {
            $path = str_replace("app.php", "", $path);
        }
        foreach ($this->routes as $route) {
            /** @var Route $route */
            if ($route->isRouteEquals($path)) {
                return $route;
            }
        }
        return null;
    }

}