<?php


namespace mpwarframework\framework\Component\Bootstrap;

use mpwarframework\framework\Component\Container\Container;
use mpwarframework\framework\Component\DataBase\PDORepository;
use mpwarframework\framework\Component\Http\Request\Request;
use mpwarframework\framework\Component\Http\Response\Response;
use mpwarframework\framework\Component\Parse\JSONParse;
use mpwarframework\framework\Component\Parse\PHPParse;
use mpwarframework\framework\Component\Parse\XMLParse;
use mpwarframework\framework\Component\Parse\YAMLParse;
use mpwarframework\framework\Component\Router\Route;
use mpwarframework\framework\Component\Router\Router;
use mpwarframework\framework\Component\View\SmartyView;
use mpwarframework\framework\Component\View\TwigView;
use ReflectionClass;

class Kernel
{

    private $config;
    private $routes;
    private $services;
    private $container;

    public function __construct($config, $routes, $services, $rootDir)
    {
        $this->config = $config;
        $this->routes = $routes;
        $this->services = $services;
        $this->container = new Container();
        $this->container->register(Container::ROUTER_CONTAINER, new Router($this->routes['routes']));
        if ($config['config']['template'] == 'twig') {
            $this->container->register(Container::TEMPLATING_CONTAINER, new TwigView($rootDir . 'templates/twig'));
        } else {
            $this->container->register(Container::TEMPLATING_CONTAINER, new SmartyView($rootDir . 'templates/smarty'));
        }
        $this->container->register(Container::REPOSITORY_CONTAINER, new PDORepository($config));

        foreach ($services['services'] as $key => $service){
            $class = $service['class'];
            $arguments = array();
            if(isset($service['arguments'])){
                foreach ($service['arguments'] as $argument){
                    $name  = $argument;
                    $name = ltrim($name, "@");
                    $param = $this->container->get($name);
                    array_push($arguments,$param);
                }
                $r = new ReflectionClass($class);
                $obj_service = $r->newInstanceArgs($arguments);
            }else{
                $obj_service = new $class();
            }
            $this->container->register($key, $obj_service);
        }
    }

    public function handle($request)
    {
        /** @var Router $router */
        $router = $this->container->get(Container::ROUTER_CONTAINER);
        /** @var Request $request */
        $path = $request->getPath();
        /** @var Route $route */
        $route = $router->getRoute($path);
        if ($route == null) {
            $response = new Response('Not found!');
            $response->setStatusCode(404);
        } else {
            $response = $this->buildResponse($route, $request);
        }
        return $response;
    }

    private function buildResponse(Route $route, $request)
    {
        $controller_name = $route->getController();
        $function_name = $route->getAction();
        $object = new $controller_name($this->container);
        return $object->$function_name($request);
    }

    public static function load($a_file_name)
    {
        $ext = pathinfo($a_file_name, PATHINFO_EXTENSION);
        $parse = null;
        switch ($ext) {
            case 'yml':
                $parse = new YAMLParse();
                break;
            case 'xml':
                $parse = new XMLParse();
                break;
            case 'json':
                $parse = new JSONParse();
                break;
            case 'php':
                $parse = new PHPParse();
                break;
            default:
                //Error
        }
        $value = array();
        $value = $parse->parse($a_file_name);
        return $value;
    }
}