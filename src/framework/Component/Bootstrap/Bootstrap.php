<?php

namespace mpwarframework\framework\Component\Bootstrap;


use mpwarframework\framework\Component\Exceptions\FileRoutingException;
use mpwarframework\framework\Component\Http\Request\Request;
use mpwarframework\framework\Component\Http\Response\Response;

class Bootstrap
{

    private $rootDir;

    private $filename_config;
    private $filename_routing;
    private $filename_services;

    private $config;
    private $routes;
    private $services;

    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
        $this->validateFilesConfiguration();
        $this->registerConfiguration();
        $this->setReporting();
    }

    private function validateFilesConfiguration()
    {
        $filename_config = $this->rootDir . "app/config.yml";
        if (!file_exists($filename_config)) {
            FileRoutingException::throwBecauseOf($filename_config);
        }
        $this->filename_config = $filename_config;
        $filename_routing = $this->rootDir . "app/routing.yml";
        if (!file_exists($filename_routing)) {
            FileRoutingException::throwBecauseOf($filename_routing);
        }
        $this->filename_routing = $filename_routing;
        $filename_services = $this->rootDir . "app/services.yml";
        if (!file_exists($filename_services)) {
            FileRoutingException::throwBecauseOf($filename_services);
        }
        $this->filename_services = $filename_services;
    }

    private function registerConfiguration()
    {
        $this->config = Kernel::load($this->filename_config);
        $this->routes = Kernel::load($this->filename_routing);
        $this->services = Kernel::load($this->filename_services);
    }

    private function setReporting()
    {
        if ($this->config['config']['environment'] == 'DEV') {
            error_reporting(E_ALL);
            ini_set('display_errors', 'On');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors', 'Off');
            ini_set('log_errors', 'On');
            ini_set('error_log', $this->rootDir . 'tmp/logs/error.log');
        }
    }

    public function run(Request $request)
    {
        $kernel = new Kernel($this->config, $this->routes, $this->services, $this->rootDir);
        /** @var Response $response */
        $response = $kernel->handle($request);
        $response->send();
    }
}