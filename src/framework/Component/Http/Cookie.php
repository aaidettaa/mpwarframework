<?php

namespace mpwarframework\framework\Component\Http;


class Cookie
{
    private $name;
    private $value;
    private $domain;
    private $path;
    private $maxAge;
    private $secure;
    private $httpOnly;

    public function __construct($name, $value)
    {
        $this->name = (string)$name;
        $this->value = (string)$value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setValue($value)
    {
        $this->value = (string)$value;
    }

    public function setMaxAge($seconds)
    {
        $this->maxAge = (int)$seconds;
    }

    public function setDomain($domain)
    {
        $this->domain = (string)$domain;
    }

    public function setPath($path)
    {
        $this->path = (string)$path;
    }

    public function setSecure($secure)
    {
        $this->secure = (bool)$secure;
    }

    public function setHttpOnly($httpOnly)
    {
        $this->httpOnly = (bool)$httpOnly;
    }

    public function getHeaderString()
    {
        $parts = [
            $this->name . '=' . rawurlencode($this->value),
            $this->getMaxAgeString(),
            $this->getExpiresString(),
            $this->getDomainString(),
            $this->getPathString(),
            $this->getSecureString(),
            $this->getHttpOnlyString(),
        ];
        $filteredParts = array_filter($parts);
        return implode('; ', $filteredParts);
    }

    private function getMaxAgeString()
    {
        if ($this->maxAge !== null) {
            return 'Max-Age=' . $this->maxAge;
        }
        return null;
    }

    private function getExpiresString()
    {
        if ($this->maxAge !== null) {
            return 'expires=' . gmdate(
                "D, d-M-Y H:i:s",
                time() + $this->maxAge
            ) . ' GMT';
        }
        return null;
    }

    private function getDomainString()
    {
        if ($this->domain) {
            return "domain=$this->domain";
        }
        return null;
    }

    private function getPathString()
    {
        if ($this->path) {
            return "path=$this->path";
        }
        return null;
    }

    private function getSecureString()
    {
        if ($this->secure) {
            return 'secure';
        }
        return null;
    }

    private function getHttpOnlyString()
    {
        if ($this->httpOnly) {
            return 'HttpOnly';
        }
        return null;
    }
}