<?php

namespace mpwarframework\framework\Component\Http\Request;


use mpwarframework\framework\Component\Exceptions\MissingServerVariableException;

class Request
{

    private $request;
    private $files;
    private $server;
    private $cookies;
    private $getParameters;
    private $postParameters;

    public function __construct(array $request,
                                array $files,
                                array $server,
                                array $cookies,
                                array $get,
                                array $post)
    {
        $this->request = $request;
        $this->files = $files;
        $this->server = $server;
        $this->cookies = $cookies;
        $this->getParameters = $get;
        $this->postParameters = $post;

    }

    public function get($name)
    {

        if (!isset($this->request[$name])) {
            throw new \Exception('El parámetro ' . $name . ' no se encuentra en la request');
        }

        return $this->request[$name];
    }

    public function getPath()
    {
        $path = strtok($this->getServerVariable('REQUEST_URI'), '?');
        $path = str_replace("mpwarapp/public/app.php", '', $path);
        return $path;
    }

    private function getServerVariable($key)
    {
        if (!array_key_exists($key, $this->server)) {
            MissingServerVariableException::throwBecauseOf($key);
        }
        return $this->server[$key];
    }

    public function getMethod()
    {
        return $this->getServerVariable('REQUEST_METHOD');
    }

    public function getHttpAccept()
    {
        return $this->getServerVariable('HTTP_ACCEPT');
    }

    public function getReferer()
    {
        return $this->getServerVariable('HTTP_REFERER');
    }

    public function getUserAgent()
    {
        return $this->getServerVariable('HTTP_USER_AGENT');
    }

    public function getIpAddress()
    {
        return $this->getServerVariable('REMOTE_ADDR');
    }

    public function isSecure()
    {
        return (array_key_exists('HTTPS', $this->server)
            && $this->server['HTTPS'] !== 'off'
        );
    }

    public function getQueryString()
    {
        return $this->getServerVariable('QUERY_STRING');
    }

    public function getParameter($key, $defaultValue = null)
    {
        if (array_key_exists($key, $this->postParameters)) {
            return $this->postParameters[$key];
        }

        if (array_key_exists($key, $this->getParameters)) {
            return $this->getParameters[$key];
        }

        return $defaultValue;
    }

    public function getQueryParameter($key, $defaultValue = null)
    {
        if (array_key_exists($key, $this->getParameters)) {
            return $this->getParameters[$key];
        }
        return $defaultValue;
    }

    public function getBodyParameter($key, $defaultValue = null)
    {
        if (array_key_exists($key, $this->postParameters)) {
            return $this->postParameters[$key];
        }
        return $defaultValue;
    }

    public function getFile($key, $defaultValue = null)
    {
        if (array_key_exists($key, $this->files)) {
            return $this->files[$key];
        }
        return $defaultValue;
    }

    public function getCookie($key, $defaultValue = null)
    {
        if (array_key_exists($key, $this->cookies)) {
            return $this->cookies[$key];
        }
        return $defaultValue;
    }

    public function getParameters()
    {
        return array_merge($this->getParameters, $this->postParameters);
    }

    public function getQueryParameters()
    {
        return $this->getParameters;
    }

    public function getBodyParameters()
    {
        return $this->postParameters;
    }

    public function getCookies()
    {
        return $this->cookies;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function getUri()
    {
        return $this->getServerVariable('REQUEST_URI');
    }

    public static function createFromGlobals(array $request,
                                             array $files,
                                             array $server,
                                             array $cookies,
                                             array $get,
                                             array $post)
    {
        return new static($request, $files, $server, $cookies, $get, $post);
    }
}