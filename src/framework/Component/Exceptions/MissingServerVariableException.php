<?php

namespace mpwarframework\framework\Component\Exceptions;


use Exception;

final class MissingServerVariableException extends Exception
{

    public static function throwBecauseOf($a_key, $a_code = 0, Exception $a_previous_exception = null)
    {
        $message = "Request meta-variable $a_key was not set.";
        throw new self($message, $a_code, $a_previous_exception);
    }

}
