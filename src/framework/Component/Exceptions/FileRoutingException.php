<?php

namespace mpwarframework\framework\Component\Exceptions;


use Exception;

final class FileRoutingException extends Exception
{

    public static function throwBecauseOf($a_key, $a_code = 0, Exception $a_previous_exception = null)
    {
        $message = "'El archivo ' . $a_key . ' no existe'";
        throw new self($message, $a_code, $a_previous_exception);
    }

}