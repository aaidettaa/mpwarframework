<?php

namespace mpwarframework\framework\Component\DataBase;

interface Repository
{
    public function find($table, array $params);

    public function findAll($table);

    public function save($table, array $params);
}