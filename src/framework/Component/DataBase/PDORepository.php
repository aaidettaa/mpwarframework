<?php


namespace mpwarframework\framework\Component\DataBase;


use PDO;

class PDORepository implements Repository
{

    private $pdo;

    public function __construct($config)
    {
        $host = $config['config']['dbhost'];
        $dbname = $config['config']['dbname'];
        $user = $config['config']['dbuser'];
        $pass = $config['config']['dbpassword'];
        $connString = "mysql:host=$host;dbname=$dbname";
        $this->pdo = new PDO($connString, $user, $pass);
        $this->pdo->setAttribute(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        );
    }

    public function find($table, array $params)
    {
        $sql = 'SELECT * FROM ' . $table;
        if (!is_null($params)) {
            $sql = $sql . ' WHERE ';
            $cuantos = 0;
            foreach ($params as $key => $value) {
                $cuantos = $cuantos + 1;
                $sql = $sql . ' ' . $key . ' = :' . $key . ' ';
                if ($cuantos != count($params)) {
                    $sql = $sql . ' AND ';
                }
            }
        }
        $stmt = $this->pdo->prepare($sql);
        if (!is_null($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindParam(':' . $key, $value);
            }
        }
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $table);
        return $stmt->fetch();
    }

    public function findAll($table)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM ' . $table);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $table);
        return $stmt->fetchAll();
    }

    public function save($table, array $params)
    {
        $sql = 'INSERT INTO ' . $table;
        if (!is_null($params)) {
            $insert = ' ';
            $values = ' ';
            $cuantos = 0;
            foreach ($params as $key => $value) {
                $cuantos = $cuantos + 1;
                $insert = $insert . ' ' . $key;
                $values = $values . ' :' . $key;
                if ($cuantos != count($params)) {
                    $insert = $insert . ', ';
                    $values = $values . ', ';
                }
            }
            $sql = $sql . ' (' . $insert . ') VALUES (' . $values . ')';
        }
        $stmt = $this->pdo->prepare($sql);
        if (!is_null($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindParam(':' . $key, $value);
            }
        }
        return $stmt->execute();
    }
}