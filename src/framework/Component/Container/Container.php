<?php


namespace mpwarframework\framework\Component\Container;


use Exception;

class Container
{

    const ROUTER = "router";
    const ROUTER_CONTAINER = "router";
    const TEMPLATING_CONTAINER = "templating";
    const REPOSITORY_CONTAINER = "repository";

    private $services;

    public function register($name, $service)
    {
        $this->services[$name] = function () use ($service) {
            return $service;
        };
    }

    public function get($name)
    {
        if (!$this->exists($name)) {
            throw new Exception("Service not registered: $name");
        }
        return $this->services[$name]();
    }

    public function exists($name)
    {
        if (!isset($this->services[$name])) {
            return false;
        }
        return true;
    }
}