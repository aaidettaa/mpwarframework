<?php

namespace mpwarframework\framework\Component\Controller;


use mpwarframework\framework\Component\Container\Container;
use mpwarframework\framework\Component\Http\Response\Response;

abstract class Controller
{

    protected $container;
    protected $response;
    protected $templating;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->response = new Response();
        $this->templating = $this->container->get(Container::TEMPLATING_CONTAINER);
    }
}