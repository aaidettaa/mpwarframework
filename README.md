# mpwarframework
MPWAR16 Framework

## Partes Relevantes
- Bootstrap
    - Se encarga de cargar todos los componentes del framework
- Container
    - Se encarga de la gestión de dependéncias
- Controller
    - Controller base del framework
- Database
    - Usa el PDO de MySQL para conectarse a la base de datos
- Http
    - Request 
    - Response
- Parse
    - Para parsear diferentes archivos de configuracion
- Router
    - Maneja todas las rutas
- View
    - Sistemas de template de Twig y Smarty